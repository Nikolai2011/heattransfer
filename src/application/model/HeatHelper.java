/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.model;

import application.model.Constants;
import java.awt.Color;

/**
 *
 * @author andrew
 */
public class HeatHelper {

    public static Color convertHeatToColor(double heatValue) {
        float heatRatio = (float) ((heatValue - Constants.TEMP_MIN) / (Constants.TEMP_MAX - Constants.TEMP_MIN));
        return blueToRed(heatRatio);
    }

    public static Color blueToRed(float heatRatio) {
        return new Color(heatRatio, 0.0f, 1.0f - heatRatio);
    }

    public static Color greedToYellow(float heatRatio) {
        //Two colors are very close, transition is barely visible.
        return new Color(heatRatio, 1.0f, 0.0f);
    }
}
