/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Element implements Runnable {

    private Thread thread;
    private boolean killThread;

    private List<Element> neighbours;
    private double currentTemp;
    private ElementController controller;

    public final int ROW;
    public final int COLUMN;

    public Element(int row, int column, double currentTemp /*, double heatConstant*/) {
        //heatConstant is not a constant value but is constant among all elements, it is shared and we 
        //can experiment with it at runtime (controller.heatConstant)
        this.ROW = row;
        this.COLUMN = column;
        this.currentTemp = currentTemp;
        neighbours = new ArrayList<>();
    }

    public void start() {
        if (thread != null) {
            throw new IllegalStateException("Thread already running.");
        }

        thread = new Thread(this);
        thread.setDaemon(false);
        thread.start();
    }

    @Override
    public void run() {
        controller.increaseThreadsRunning();
        while (!killThread) {
            try {
                Thread.sleep(controller.elSleepTime);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

            if (controller != null && controller.pauseThread) {
                continue;
            }

            try {
                transferSomeHeat();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        controller.decreaseThreadsRunning();
        thread = null;
    }

    private void transferSomeHeat() {
        if (controller.heatTransferAlgorithm == Constants.DEFAULT_ALG) {
            useDefaultHeatTransferAlgorithm();
        } else if (controller.heatTransferAlgorithm == Constants.CUSTOM_ALG) {
            Object permission = controller.obtainHeatTransferPermission(this);
            if (permission != null) {
                synchronized (permission) {
                    //At this point, no NEARBY neighbours transfer or receive heat from other elements. 
                    //So this element is free to access its neighbours.
                    //Permission is 'this' object, synchronization prohibits the mouse from tampering in applyTempToElement.
                    useCustomHeatTransferAlgorithm();
                }
                controller.returnHeatTransferPermission(this);
            }
        }
    }

    private void useDefaultHeatTransferAlgorithm() {
        //ALGORITHM AS IN THE TASK DESCRIPTION (one-way heat transfer)
        double totalTemp = 0.0d;
        double avgTemp = 0.0d;

        for (Element element : neighbours) {
            totalTemp += element.getTemperature();
        }

        synchronized (this) {
            if (neighbours.size() > 0) {
                avgTemp = totalTemp / neighbours.size();
                currentTemp += (avgTemp - currentTemp) * (controller.elementHeatConstant); //AUT solution divides this result by 5.0. My solution used greater delay for timer
            }
        }
    }

    public double getTemperature() {
        synchronized (this) {
            return currentTemp;
        }
    }

    public double applyTempToElement(double appliedTemp) {
        double diff = 0.0d;
        synchronized (this) {
            if (appliedTemp >= Constants.TEMP_MIN && appliedTemp <= Constants.TEMP_MAX) {
                diff = (appliedTemp - currentTemp) * controller.mouseHeatConstant;
                this.currentTemp += diff;
            } else {
                System.err.println("Illigal temperature setting");
                this.currentTemp = 0.0d;
            }
        }
        return diff;
    }

    private void moveHeat(double heatValue) {
        //Used by custom ALGORITHM 1 (two-way heat transfer)
        //synchronized from parent method
        double result = currentTemp + heatValue;
        if (result <= Constants.TEMP_MAX && result >= Constants.TEMP_MIN) {
            this.currentTemp = result;
        } else {
            //I put a breakpoint here. Hopefully, this should never happen.
            System.err.println("Illigal temperature setting");
            this.currentTemp = 0.0d;
        }
    }

    private void useCustomHeatTransferAlgorithm() {
        //ALGORITHM 1 (two-way heat transfer)
        //Synchronized from parent method
        //Due to two-way heat transfer, when each element transfers heat to others and modify its own heat,
        //the exact amount of heat/energy is preserved
        double totalHigherHeatDiff = 0.0d;
        double totalLowerHeatDiff = 0.0d;
        int lowerHeatElementsCount = 0;
        int higherHeatElementsCount = 0;
        double diff;
        for (Element element : neighbours) {
            diff = Math.abs(this.getTemperature() - element.getTemperature());
            if (element.getTemperature() < this.currentTemp) {
                lowerHeatElementsCount++;
                totalLowerHeatDiff += diff;
            } else if (element.getTemperature() > this.currentTemp) {
                higherHeatElementsCount++;
                totalHigherHeatDiff += diff;
            }
        }

        double incomeHeat = 0.0d;
        double outgoneHeat = 0.0d;

        double avgOutgoingDeltaT = (lowerHeatElementsCount > 0) ? totalLowerHeatDiff / lowerHeatElementsCount : 0.0d;
        double avgIncomingDeltaT = (higherHeatElementsCount > 0) ? totalHigherHeatDiff / higherHeatElementsCount : 0.0d;

        //Division by 2 since the operation will include reducing current temperature and increasing temperature of the target
        avgOutgoingDeltaT /= 2;
        avgIncomingDeltaT /= 2;

        double heatTransferResult;
        for (Element element : neighbours) {
            diff = Math.abs(this.getTemperature() - element.getTemperature());
            if (element.getTemperature() < this.currentTemp) {
                heatTransferResult
                        = (avgOutgoingDeltaT * (diff / totalLowerHeatDiff))
                        * (diff / (Constants.TEMP_MAX - Constants.TEMP_MIN))
                        * controller.elementHeatConstant;

                element.moveHeat(heatTransferResult);
                outgoneHeat += heatTransferResult;
            } else if (element.getTemperature() > this.currentTemp) {
                heatTransferResult
                        = -(avgIncomingDeltaT * (diff / totalHigherHeatDiff))
                        * (diff / (Constants.TEMP_MAX - Constants.TEMP_MIN))
                        * controller.elementHeatConstant;

                element.moveHeat(heatTransferResult);
                incomeHeat += -heatTransferResult;
            }

            /*
                heatTransferResult explained...
                
                1.
                (avgOutgoingDeltaT * (diff / totalLowerHeatDiff)) - calculates how much energy/heat the target element deserves
                in current temperature transaction. This ensures that those elements with greater difference in temperature will
                get the most of current element's temperature.
                
                2.
                (diff / (TEMP_MAX - TEMP_MIN)) - makes heat transfer slower when there is little difference in temperatures
                of nearby elements
                
                3.
                controller.heatConstant - how fast heat transfers, basically point 2. does a similar thing but 2. also adds the effect of 
                slowing down heat distribution over large distance (e.g. over 10 elements).
             */
        }

        currentTemp += incomeHeat;
        currentTemp -= outgoneHeat;
    }

    public Color getElementColor() {
        //Canvas and timer uses this to draw elements. Canvas updates
        //quite often so sync is not needed (it would slow down performance).
        return HeatHelper.convertHeatToColor(currentTemp);
    }

    public void setElementController(ElementController controller) {
        this.controller = controller;
    }

    public void killThread() {
        this.killThread = true;
    }

    public void addNeighbour(Element neighbour) {
        this.neighbours.add(neighbour);
    }

//    See ElementTester class
//    public static void main(String[] args) {
//    }
}
