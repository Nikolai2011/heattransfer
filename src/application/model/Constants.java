/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.model;

import java.awt.Color;

public class Constants {

    public static final double TEMP_MAX = 100.0d;
    public static final double TEMP_MIN = 0.0d;
    public static final double TEMP_MEDIUM = (TEMP_MAX - TEMP_MIN) / 2.0d + TEMP_MIN;

    public static final int DEFAULT_ALG = 0;
    public static final int CUSTOM_ALG = 1;
    
    public static final Color COLOR_CELL_BORDER = Color.BLACK;
    
    public static final double HEAT_CONSTANT_MOUSE = 0.2;
    public static final double HEAT_CONSTANT_ELEMENT = 0.7;
    
    public static final int REDRAW_SPEED = 5;
    public static final int MOUSE_HEAT_TRANSFER_SPEED = 10;
    public static final int ELEMENT_SLEEP_TIME = 5;
}
