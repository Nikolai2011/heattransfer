/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.model;

public class ElementController {
    public interface IAllThreadsKilledListener{
        void onAllThreadsKilled();
    }
    
    private Element[][] elements;
    private Object[][] busyElements; //Elements that are permitted to transfer heat to others

    public int elSleepTime = 10;
    public boolean pauseThread;
    public double mouseHeatConstant;
    public double elementHeatConstant;
    public int heatTransferAlgorithm = Constants.DEFAULT_ALG;
    
    private IAllThreadsKilledListener threadsListener;
    private int threadsRunning = 0;

    public ElementController() {
    }

    public synchronized Object obtainHeatTransferPermission(Element element) {
        //Synchronization for custom algorithm
        //It is not used by default algorithm
        //Only custom algorithm requires a reservation of neighbour elements so that only this element can transfer heat to/from
        //its neighbours and no one else.
        //If some element which is 2 cells away (or 1 cell diagonally) is transferring heat, this element is not allowed to
        //begin its own heat transfer (since they may accidentally read/modify the temperature of the element in between them).

        if (elements == null) {
            return null;
        }
        
        boolean nearbyHeatTransferDetected = false;
        for (int offset = -2; offset <= 2; offset++) {
            if (offset != 0 && (isElementBusy(element, element.ROW + offset, element.COLUMN)
                    || isElementBusy(element, element.ROW, element.COLUMN + offset))) {
                //Check 2 nearby cells horizontally/vertically in all 4 directions, if there are busy elements - permission is denied
                nearbyHeatTransferDetected = true;
                break;
            } 
            else if (Math.abs(offset) == 1 && isElementBusy(element, element.ROW + offset, element.COLUMN + offset)) {
                //Check 1 nearby cell diagonallly in all 4 directions
                nearbyHeatTransferDetected = true;
                break;
            }
        }

        if (!nearbyHeatTransferDetected) {
            //This element is allowed permission and is marked as busy (actively transferring heat). Neighbours will not be marked.
            busyElements[element.ROW][element.COLUMN] = element;
        }

        return (nearbyHeatTransferDetected) ? null : element;
    }

    public synchronized void returnHeatTransferPermission(Element element) {
        if (busyElements != null) {
            busyElements[element.ROW][element.COLUMN] = null;
        }
    }

    private boolean isElementBusy(Element element, int row, int col) {
        boolean busy = false;

        try {
            if (busyElements[row][col] != null) {
                busy = true;
            }
        } catch (IndexOutOfBoundsException ex) {

        } catch (Exception ex) {
            //May throw exceptions when elements array is recreated but this thread has yet to be killed (such as when changing grid size)
            ex.printStackTrace();
        }

        return busy;
    }

    public void setElements(Element[][] elements) {
        this.elements = elements;
        busyElements = (elements == null) ? null : new Object[elements.length][elements[0].length];
    }
    
    public synchronized void increaseThreadsRunning() {
        threadsRunning++;
    }
    
    public synchronized void decreaseThreadsRunning() {
        threadsRunning--;
        if (threadsRunning == 0 && this.threadsListener != null) {
            threadsListener.onAllThreadsKilled();
        } else if (threadsRunning < 0) {
            System.err.println("Unexpected threads running: " + threadsRunning);
        }
    }

    public void setThreadsListener(IAllThreadsKilledListener threadsListener) {
        this.threadsListener = threadsListener;
        if (threadsListener != null && threadsRunning == 0) {
            threadsListener.onAllThreadsKilled();
        }
    }
}
