/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import application.model.Constants;
import application.model.Element;
import application.model.ElementController;
import application.ui.AppView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import application.ui.MyCanvas;

public class AppController {

    private AppView appView;
    private HotPlateGUI hotPlateGUI;
    private MyCanvas canvas;

    public Timer timerDraw;
    public Timer timerMouseHeat;
    private Timer timerStatsDisplay;
    private ElementController elementController;
    private Element[][] elements;
    private double mouseTemperature;
    private int columnsCount = 30;
    private int rowsCount = 30;

    private double expectedTotalTemp;
    private double actualTotalTemp;

    public AppController(HotPlateGUI hotPlateGUI) {
        this.hotPlateGUI = hotPlateGUI;

        elementController = new ElementController();  //Before initElements
        elementController.mouseHeatConstant = Constants.HEAT_CONSTANT_MOUSE;
        elementController.elementHeatConstant = Constants.HEAT_CONSTANT_ELEMENT;
        elementController.elSleepTime = Constants.ELEMENT_SLEEP_TIME;
        elementController.heatTransferAlgorithm = Constants.CUSTOM_ALG;
        elementController.setElements(elements);

        timerDraw = new Timer(Constants.REDRAW_SPEED, null);
        timerMouseHeat = new Timer(Constants.MOUSE_HEAT_TRANSFER_SPEED, null);
        timerStatsDisplay = new Timer(1000, null);
        mouseTemperature = Constants.TEMP_MIN;
        appView = new AppView(this, hotPlateGUI); //After mouse temper is set
        canvas = appView.getCanvas();
        canvas.setDrawBorder(true);
        canvas.setDrawHeatValues(false);

        initElements();
        canvas.setElements(elements);

        attachHandlers();
        appView.updateGUI();
        appView.adjustControlValues();

        timerDraw.start();
        timerMouseHeat.start();
        timerStatsDisplay.start();
    }

    private void initElements() {
        elementController.pauseThread = true;
        elements = new Element[rowsCount][columnsCount];
        elementController.setElements(elements);

        double initialHeat = Constants.TEMP_MAX;
        if (appView != null) {
            if (appView.getjComboBoxChoosePlate().getSelectedIndex() == 0) {
                initialHeat = Constants.TEMP_MIN;
            } else if (appView.getjComboBoxChoosePlate().getSelectedIndex() == 1) {
                initialHeat = Constants.TEMP_MEDIUM;
            } else if (appView.getjComboBoxChoosePlate().getSelectedIndex() == 2) {
                initialHeat = Constants.TEMP_MAX;
            }
        }
        expectedTotalTemp = initialHeat * rowsCount * columnsCount;

        int row, col;
        for (row = 0; row < rowsCount; row++) {
            for (col = 0; col < columnsCount; col++) {
                elements[row][col] = new Element(row, col, initialHeat);
            }
        }

        Element el;
        for (row = 0; row < rowsCount; row++) {
            for (col = 0; col < columnsCount; col++) {
                el = elements[row][col];

                if (row > 0) {
                    el.addNeighbour(elements[row - 1][col]);
                }
                if (col > 0) {
                    el.addNeighbour(elements[row][col - 1]);
                }

                if (row < rowsCount - 1) {
                    el.addNeighbour(elements[row + 1][col]);
                }
                if (col < columnsCount - 1) {
                    el.addNeighbour(elements[row][col + 1]);
                }

                el.setElementController(elementController);
                el.start();
            }
        }

        elementController.pauseThread = false;
    }

    private void attachHandlers() {
        timerDraw.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.repaint();
            }
        });

        timerMouseHeat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (canvas.isMousePressed()) {
                    Element el = canvas.getElementAtMousePosition();

                    if (el != null) {
                        if (elementController.heatTransferAlgorithm == Constants.CUSTOM_ALG) {
                            Object permission = elementController.obtainHeatTransferPermission(el);
                            if (permission != null) {
                                //No need to synchronize - element is not targeted by other elements and will not be
                                //in the future while we are heating it up/down with a mouse. Obtaining permission is synchronized, though.
                                expectedTotalTemp += el.applyTempToElement(mouseTemperature);
                                elementController.returnHeatTransferPermission(el);
                            }
                        } else {
                            expectedTotalTemp += el.applyTempToElement(mouseTemperature);
                        }
                    }
                }

            }
        });

        timerStatsDisplay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hotPlateGUI.jLabelExpectedHeat.setText(String.format("Expected Total Heat: %.2f", expectedTotalTemp));

                actualTotalTemp = 0.0d;
                for (Element[] row : elements) {
                    for (Element el : row) {
                        actualTotalTemp += el.getTemperature();
                    }
                }

                hotPlateGUI.jLabelActualHeat.setText(String.format("Actual Total Heat:   %.2f", actualTotalTemp));
                
                hotPlateGUI.jLabelElementSleepTime.setText("Element Sleep Time (ms): " + elementController.elSleepTime);
                hotPlateGUI.jLabelMouseHeatTransferSpeed.setText("Mouse Heat Transfer Speed: " + timerMouseHeat.getDelay());
                hotPlateGUI.jLabelRedrawSpeed.setText("Redraw Speed: " + timerDraw.getDelay());
            }
        });

        ActionListener jCheckBoxActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == hotPlateGUI.jCheckBoxDrawHeatValues) {
                    canvas.setDrawHeatValues(hotPlateGUI.jCheckBoxDrawHeatValues.isSelected());
                } else if (e.getSource() == hotPlateGUI.jCheckBoxDrawBorder) {
                    canvas.setDrawBorder(hotPlateGUI.jCheckBoxDrawBorder.isSelected());
                }
            }
        };
        hotPlateGUI.jCheckBoxDrawHeatValues.addActionListener(jCheckBoxActionListener);
        hotPlateGUI.jCheckBoxDrawBorder.addActionListener(jCheckBoxActionListener);

        ActionListener btThreadControlActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == hotPlateGUI.jButtonResume) {
                    elementController.pauseThread = false;
                } else if (e.getSource() == hotPlateGUI.jButtonPause) {
                    elementController.pauseThread = true;
                }
            }
        };

        hotPlateGUI.jButtonResume.addActionListener(btThreadControlActionListener);
        hotPlateGUI.jButtonPause.addActionListener(btThreadControlActionListener);
    }

    public void requestNewGrid() {
        timerDraw.stop();
        canvas.setElements(null);
        killAllThreads();

        elementController.setThreadsListener(new ElementController.IAllThreadsKilledListener() {
            @Override
            public void onAllThreadsKilled() {
                elementController.setElements(null);
                initElements();
                canvas.setElements(elements);
                timerDraw.start();
            }
        });
    }

    private void killAllThreads() {
        for (Element[] row : elements) {
            for (Element el : row) {
                el.killThread();
            }
        }
    }

    public ElementController getElementController() {
        return elementController;
    }

    public int getColumnsCount() {
        return columnsCount;
    }

    public int getRowsCount() {
        return rowsCount;
    }

    public void setColumnsCount(int columnsCount) {
        this.columnsCount = columnsCount;
    }

    public void setRowsCount(int rowsCount) {
        this.rowsCount = rowsCount;
    }

    public double getMouseTemperature() {
        return mouseTemperature;
    }

    public void setMouseTemperature(double mouseTemperature) {
        this.mouseTemperature = mouseTemperature;
    }
}
