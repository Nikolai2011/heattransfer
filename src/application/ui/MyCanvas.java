/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.ui;

import application.model.Constants;
import application.model.Element;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;

/**
 *
 * @author andrew
 */
public class MyCanvas extends JPanel {

    public final float BORDER_SIZE = 1.0f;

    private Element[][] elements;
    private boolean isMousePressed = false;
    private Point mousePosition;
    private double elHeight; //Element Height/Width
    private double elWidth;
    private boolean drawHeatValues; //Whether grid should display temperatures of elements
    private boolean drawBorder;
    
    public Color borderColor = Constants.COLOR_CELL_BORDER;

    public MyCanvas() {
        attachHandlers();
    }

    private void attachHandlers() {
        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                onMouseEvent(e);
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                onMouseEvent(e);
            }
        });
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                onMouseEvent(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                onMouseEvent(e);
            }
        });
    }

    private void onMouseEvent(MouseEvent e) {
        mousePosition = e.getPoint();

        if (e.getButton() == MouseEvent.BUTTON1) {
            if (e.getID() == MouseEvent.MOUSE_PRESSED) {
                isMousePressed = true;
            } else if (e.getID() == MouseEvent.MOUSE_RELEASED) {
                isMousePressed = false;
            }
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (elements == null) {
            return;
        }

        Graphics2D g2 = (Graphics2D) g;

//        RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
//                RenderingHints.VALUE_ANTIALIAS_ON);
//        rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
//        g2.setRenderingHints(rh);
        g2.setStroke(new BasicStroke(BORDER_SIZE));
        elHeight = ((double) this.getSize().height) / elements.length;
        elWidth = ((double) this.getSize().width) / elements[0].length;

        for (int row = 0; row < elements.length; row++) {
            for (int col = 0; col < elements[0].length; col++) {
                if (elements[row][col] == null) {
                    System.err.println("Null Element at row " + row + "; col " + col);
                    continue;
                }

                g2.setColor(elements[row][col].getElementColor());
                if (drawBorder) {
                    g2.fill(new Rectangle2D.Double(col * elWidth, row * elHeight, elWidth, elHeight));
                } else {
                    g2.fill(new Rectangle((int) (col * elWidth), (int) (row * elHeight), (int) (elWidth + 1), (int) (elHeight + 1)));
                }

                if (drawBorder) {
                    g2.setColor(borderColor);
                    double borderWidth = (col == elements[0].length - 1) ? elWidth - BORDER_SIZE : elWidth;
                    double borderHeight = (row == elements.length - 1) ? elHeight - BORDER_SIZE : elHeight;
                    g2.draw(new Rectangle2D.Double(col * elWidth, row * elHeight, borderWidth, borderHeight));
                }

                if (drawHeatValues) {
                    g2.setColor(Color.WHITE);
                    String temp = String.format("%.1f", elements[row][col].getTemperature());
                    g2.drawString(temp,
                            (float) (col * elWidth + elWidth / 2 - g.getFontMetrics().stringWidth(temp) / 2),
                            (float) (row * elHeight + elHeight / 2));
                }
            }
        }
    }

    public Element getElementAtMousePosition() {
        Element el = null;

        if (elements != null && this.contains(mousePosition)) {
            int x = mousePosition.x;
            int y = mousePosition.y;

            el = elements[(int) (y / elHeight)][(int) (x / elWidth)];
        }

        return el;
    }

    public boolean isMousePressed() {
        return isMousePressed;
    }

    public boolean isDrawHeatValues() {
        return drawHeatValues;
    }

    public void setDrawHeatValues(boolean drawHeatValues) {
        this.drawHeatValues = drawHeatValues;
    }

    public void setElements(Element[][] elements) {
        this.elements = elements;
    }

    public void setDrawBorder(boolean drawBorder) {
        this.drawBorder = drawBorder;
    }

    public boolean isDrawBorder() {
        return drawBorder;
    }
}
