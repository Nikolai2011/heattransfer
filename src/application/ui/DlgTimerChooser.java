/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.ui;

import application.HotPlateGUI;
import application.model.ElementController;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author andrew
 */
public class DlgTimerChooser extends JDialog {

    ElementController elController;
    Timer timerRedraw;
    Timer timerMouseHeat;

    JTextArea jTextAreaInfo;
    JLabel jLabelElementSleepTime;
    JLabel jLabelRedrawSpeed;
    JLabel jLabelMouseHeatTransferSpeed;
    JLabel jLabelMouseHeatConstant;
    JLabel jLabelElementHeatConstant;
    JSlider jSliderElementSleepTime;
    JSlider jSliderRedrawSpeed;
    JSlider jSliderMouseHeatTransferSpeed;
    JSlider jSliderMouseHeatConstant;
    JSlider jSliderElementHeatConstant;
    JPanel jPanelMain;
    HotPlateGUI hotPlateGUI;

    public DlgTimerChooser(HotPlateGUI parent, ElementController elController, Timer timerDraw, Timer timerMouseHeat) {
        super(parent, false);
        hotPlateGUI = parent;
        this.elController = elController;
        this.timerRedraw = timerDraw;
        this.timerMouseHeat = timerMouseHeat;
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        setTitle("Timer Controller");

        initUI();
        attachHandlers();
        loadLayout();

        pack();
        setLocationRelativeTo(parent);
        setVisible(true);
    }

    private void initUI() {
        jPanelMain = new JPanel(new GridBagLayout());
        jTextAreaInfo = new JTextArea(
                "1) MHTS: 100 initial | 10 custom | 5 scratch | 5 default\n"
                + "2) MHC: 0.7 initial | 0.2 custom | 1.0 scratch | 1 default\n"
                + "3) ESL: 20 initial | 5 custom | 100 scratch | 25 default\n"
                        + "4) EHC: 0.7 initial | 0.7 custom | 0.15 scratch | 1 default\n"
                        + "5) RS: 20 initial | 5 custom | 100 or 5 scratch | 5 default"
        );
        jTextAreaInfo.setEditable(false);

        jLabelElementSleepTime = new JLabel("3) Element Sleep Time: " + elController.elSleepTime);
        jLabelRedrawSpeed = new JLabel("5) Redraw Speed: " + timerRedraw.getDelay());
        jLabelMouseHeatTransferSpeed = new JLabel("1) Mouse Heat Transfer Speed: " + timerMouseHeat.getDelay());
        jLabelMouseHeatConstant = new JLabel("2) Mouse Heat Constant: " + elController.mouseHeatConstant);
        jLabelElementHeatConstant = new JLabel("4) Element Heat Constant: " + elController.elementHeatConstant);
        jSliderElementSleepTime = new JSlider();
        jSliderElementSleepTime.setMinimum(5);
        jSliderElementSleepTime.setMaximum(100);
        jSliderElementSleepTime.setValue(elController.elSleepTime);

        jSliderMouseHeatTransferSpeed = new JSlider();
        jSliderMouseHeatTransferSpeed.setMinimum(5);
        jSliderMouseHeatTransferSpeed.setMaximum(100);
        jSliderMouseHeatTransferSpeed.setValue(timerMouseHeat.getDelay());

        jSliderRedrawSpeed = new JSlider();
        jSliderRedrawSpeed.setMinimum(5);
        jSliderRedrawSpeed.setMaximum(100);
        jSliderRedrawSpeed.setValue(timerRedraw.getDelay());

        jSliderMouseHeatConstant = new JSlider();
        jSliderMouseHeatConstant.setInverted(true);
        jSliderMouseHeatConstant.setMinimum(5);
        jSliderMouseHeatConstant.setMaximum(100);
        jSliderMouseHeatConstant.setValue((int)(elController.mouseHeatConstant*100.0d));

        jSliderElementHeatConstant = new JSlider();
        jSliderElementHeatConstant.setInverted(true);
        jSliderElementHeatConstant.setMinimum(5);
        jSliderElementHeatConstant.setMaximum(100);
        jSliderElementHeatConstant.setValue((int)(elController.elementHeatConstant*100.0d));
    }

    private void attachHandlers() {
        jSliderRedrawSpeed.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                timerRedraw.setDelay(jSliderRedrawSpeed.getValue());
                jLabelRedrawSpeed.setText("5) Redraw Speed: " + timerRedraw.getDelay());
            }
        });

        jSliderElementSleepTime.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                elController.elSleepTime = jSliderElementSleepTime.getValue();
                jLabelElementSleepTime.setText("3) Element Sleep Time: " + elController.elSleepTime);
            }
        });

        jSliderMouseHeatTransferSpeed.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                timerMouseHeat.setDelay(jSliderMouseHeatTransferSpeed.getValue());
                jLabelMouseHeatTransferSpeed.setText("1) Mouse Heat Transfer Speed: " + timerMouseHeat.getDelay());
            }
        });
        
        jSliderMouseHeatConstant.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                elController.mouseHeatConstant = jSliderMouseHeatConstant.getValue()/100.0d;
                String display = "2) Mouse Heat Constant: " + String.format("%.2f", elController.mouseHeatConstant);
                hotPlateGUI.jLabelMouseHeatConstant.setText(display);
                jLabelMouseHeatConstant.setText(display);
            }
        });
        
        jSliderElementHeatConstant.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                elController.elementHeatConstant = jSliderElementHeatConstant.getValue()/100.0d;
                String display = "4) Element Heat Constant: " + String.format("%.2f", elController.elementHeatConstant);
                jLabelElementHeatConstant.setText(display);
                hotPlateGUI.jLabelElementHeatConstant.setText(display);
            }
        });
    }

    private void loadLayout() {
        int anchor = GridBagConstraints.CENTER;
        int fill = GridBagConstraints.HORIZONTAL;
        Insets insets = new Insets(5, 5, 0, 5);
        Insets insets2 = new Insets(2, 5, 5, 5);

        jPanelMain.add(jLabelMouseHeatTransferSpeed, new GridBagConstraints(0, 0, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));
        jPanelMain.add(jSliderMouseHeatTransferSpeed, new GridBagConstraints(0, 1, 1, 1, 1.0d, 0.0d, anchor, fill, insets2, 0, 0));
        jPanelMain.add(jLabelMouseHeatConstant, new GridBagConstraints(0, 2, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));
        jPanelMain.add(jSliderMouseHeatConstant, new GridBagConstraints(0, 3, 1, 1, 1.0d, 0.0d, anchor, fill, insets2, 0, 0));
        jPanelMain.add(jLabelElementSleepTime, new GridBagConstraints(0, 4, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));
        jPanelMain.add(jSliderElementSleepTime, new GridBagConstraints(0, 5, 1, 1, 1.0d, 0.0d, anchor, fill, insets2, 0, 0));
        jPanelMain.add(jLabelElementHeatConstant, new GridBagConstraints(0, 6, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));
        jPanelMain.add(jSliderElementHeatConstant, new GridBagConstraints(0, 7, 1, 1, 1.0d, 0.0d, anchor, fill, insets2, 0, 0));
        jPanelMain.add(jLabelRedrawSpeed, new GridBagConstraints(0, 8, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));
        jPanelMain.add(jSliderRedrawSpeed, new GridBagConstraints(0, 9, 1, 1, 1.0d, 0.0d, anchor, fill, insets2, 0, 0));
        jPanelMain.add(jTextAreaInfo, new GridBagConstraints(0, 10, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));

        getContentPane().add(jPanelMain, BorderLayout.CENTER);
    }
}
