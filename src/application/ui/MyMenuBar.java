/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.ui;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MyMenuBar extends JMenuBar {

    JMenu menuHeatAlgorithmControl;
//    JMenuItem menuItemCustomAlgorithm;
//    JMenuItem menuItemDefaultAlgorithm;
    JMenuItem menuItemChooseAlgorithm;
    JMenuItem menuItemTimeControl;
    JMenuItem menuItemBorderColor;

    public MyMenuBar() {
        initComponents();
        loadComponents();
    }

    private void initComponents() {
        menuHeatAlgorithmControl = new JMenu("More Options");
        menuItemChooseAlgorithm = new JMenuItem("Choose Algorithm");
        menuItemTimeControl = new JMenuItem("Time Control");
        menuItemBorderColor = new JMenuItem("Border Color");

//        menuItemCustomAlgorithm = new JMenuItem("Custom Algorithm");
//        menuItemDefaultAlgorithm = new JMenuItem("Default Algorithm");
    }

    private void loadComponents() {
//        menuHeatAlgorithmControl.add(menuItemCustomAlgorithm);
//        menuHeatAlgorithmControl.add(menuItemDefaultAlgorithm);

        menuHeatAlgorithmControl.add(menuItemChooseAlgorithm);
        menuHeatAlgorithmControl.add(menuItemTimeControl);
        menuHeatAlgorithmControl.add(menuItemBorderColor);
        this.add(menuHeatAlgorithmControl);
    }
}
