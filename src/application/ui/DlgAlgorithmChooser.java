/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.ui;

import application.model.Constants;
import application.model.ElementController;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import java.awt.event.ActionListener;

/**
 * Called from AppView.attachHandlers (myMenuBar.menuItemChooseAlgorithm)
 */
public class DlgAlgorithmChooser extends JDialog {

    JPanelAlgorithmChooser content;
    int chosenAlgorithm;

    public DlgAlgorithmChooser(JFrame parent, int initialAlg) {
        super(parent, true);
        setTitle("Heat Transfer Algorithm");

        chosenAlgorithm = initialAlg;
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        initUI();
        setLayout(new BorderLayout());
        setContentPane(content);

        if (chosenAlgorithm == Constants.DEFAULT_ALG) {
            content.jRadioButtonDefaultAlg.setSelected(true);
        } else if (chosenAlgorithm == Constants.CUSTOM_ALG) {
            content.jRadioButtonCustomAlg.setSelected(true);
        }

        pack();
        setLocationRelativeTo(parent);
        setVisible(true);
    }

    private void initUI() {
        content = new JPanelAlgorithmChooser();
        content.jTextAreaDefaultAlgorithm.setEditable(false);
        content.jTextAreaDefaultAlgorithm.setText("Default algorithm as specified in the task description. "
                + "Each element updates its own temperature based on nearby elements. Total heat is not preserved and some heat may be lost or gained."
                + "\n\nWorks best at ~90?ms (auto sets) sleep time (noticeable delay is needed + small grid size).");

        content.jTextAreaCustomAlgorithm.setEditable(false);
        content.jTextAreaCustomAlgorithm.setText("Algorithm that makes each element transfer heat to/from nearby elements in addition to updating"
                + " its own temperature. Total heat is preserved and should not fluctuate while at rest."
                + " Also it adds the effect of slower heat dispersion over large distances. E.g. more time will pass before"
                + " heat at the bottom-left corner reaches top-right corner. "
                + "\n\nWorks best at ~" + Constants.ELEMENT_SLEEP_TIME + "ms (auto sets) (no delay is needed)"
                + "\n\n*To observe the difference between algorithms: pause elements -> heat up or cool down 1-5 elements"
                + " -> start elements and watch total heat.");

        ActionListener actionListenerChoice = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == content.jRadioButtonDefaultAlg) {
                    chosenAlgorithm = Constants.DEFAULT_ALG;
                } else if (e.getSource() == content.jRadioButtonCustomAlg) {
                    chosenAlgorithm = Constants.CUSTOM_ALG;
                }
            }
        };

        content.jRadioButtonDefaultAlg.addActionListener(actionListenerChoice);
        content.jRadioButtonCustomAlg.addActionListener(actionListenerChoice);

        content.jButtonApply.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                dispose();
            }
        });
    }

    public int getChosenAlgorithm() {
        return chosenAlgorithm;
    }
}
