/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application.ui;

import application.AppController;
import application.HotPlateGUI;
import application.model.Constants;
import application.model.ElementController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class AppView {

    private AppController app;
    private ElementController elController;

    private HotPlateGUI hotPlateGUI;
    private MyMenuBar myMenuBar;
    private MyCanvas canvas;
    private JComboBox jComboBoxRows;
    private JComboBox jComboBoxColumns;
    private JComboBox jComboBoxChoosePlate;
    private JSlider jSliderMouseHeat;
    private JSlider jSliderHeatConstant;
    private JLabel jLabelMouseTemperature;
    private JLabel jLabelHeatConstant;
    private JLabel jLabelElementSleepTime;

    public AppView(AppController app, HotPlateGUI hotPlateGUI) {
        this.app = app;
        this.hotPlateGUI = hotPlateGUI;
        this.elController = app.getElementController();
        initUI();
        attachHandlers();
    }

    private void initUI() {
        myMenuBar = new MyMenuBar();
        hotPlateGUI.setJMenuBar(myMenuBar);

        canvas = new MyCanvas();
        canvas.setBackground(Color.red);
        hotPlateGUI.jPanelCanvas.setLayout(new BorderLayout());
        hotPlateGUI.jPanelCanvas.add(canvas, BorderLayout.CENTER);

        jComboBoxRows = hotPlateGUI.jComboBoxRows;
        jComboBoxRows.setSelectedItem(app.getRowsCount());
        jComboBoxColumns = hotPlateGUI.jComboBoxColumns;
        jComboBoxColumns.setSelectedItem(app.getColumnsCount());

        jComboBoxChoosePlate = hotPlateGUI.jComboBoxChoosePlate;
        jComboBoxChoosePlate.setSelectedIndex(2);

        jSliderMouseHeat = hotPlateGUI.jSliderMouseTemperature;
        double valueScale = 3.0d;
        jSliderMouseHeat.setMinimum((int) Constants.TEMP_MIN);
        jSliderMouseHeat.setMaximum((int) (valueScale * (Constants.TEMP_MAX - Constants.TEMP_MIN) + Constants.TEMP_MIN));
        jSliderMouseHeat.setValue((int) (valueScale * app.getMouseTemperature()));

        jLabelMouseTemperature = hotPlateGUI.jLabelMouseTemp;
        jLabelHeatConstant = hotPlateGUI.jLabelMouseHeatConstant;
        jLabelElementSleepTime = hotPlateGUI.jLabelElementSleepTime;
    }

    public void attachHandlers() {
        ActionListener actionListenerGridSizeChooser = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int rows = Integer.parseInt(jComboBoxRows.getSelectedItem().toString());
                    int columns = Integer.parseInt(jComboBoxColumns.getSelectedItem().toString());

                    if (rows > 50) {
                        hotPlateGUI.jLabelRows.setText("Too many rows...");
                    } else {
                        app.setRowsCount(rows);
                        hotPlateGUI.jLabelRows.setText("Rows");
                    }

                    if (columns > 50) {
                        hotPlateGUI.jLabelColumns.setText("Too many columns...");
                    } else {
                        app.setColumnsCount(columns);
                        hotPlateGUI.jLabelColumns.setText("Columns");
                    }

                    app.requestNewGrid();

                } catch (Exception ex) {
                    System.out.println("Invalid Input: " + ex.getMessage());
                    //ex.printStackTrace();
                }
            }
        };

        myMenuBar.menuItemChooseAlgorithm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                //Create Dialog
                DlgAlgorithmChooser dlg = new DlgAlgorithmChooser(hotPlateGUI, elController.heatTransferAlgorithm);

                int prevAlg = elController.heatTransferAlgorithm;
                int chosenAlgorithm = dlg.getChosenAlgorithm();
                elController.heatTransferAlgorithm = chosenAlgorithm;
                if (chosenAlgorithm != prevAlg) {
//                    if (chosenAlgorithm == Constants.DEFAULT_ALG) {
//                        elController.elSleepTime = Constants.BEST_RUN_FREQUENCY_DEFAULT;
//                    } else if (chosenAlgorithm == Constants.CUSTOM_ALG) {
//                        elController.elSleepTime = Constants.BEST_RUN_FREQUENCY_CUSTOM;
//                    }
                    app.requestNewGrid(); //Reset actual/expected heat statistics
                }

                dlg.dispose();
            }
        });

        myMenuBar.menuItemTimeControl.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DlgTimerChooser dlg = new DlgTimerChooser(hotPlateGUI, elController, app.timerDraw, app.timerMouseHeat);
                //No need to dispose since the dialog is modal
//            dlg.dispose();
            }
        });

        myMenuBar.menuItemBorderColor.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color newColor = JColorChooser.showDialog(hotPlateGUI, "Choose Border Color", canvas.borderColor);
                if (newColor != null) {
                    canvas.borderColor = newColor;
                }
            }
        });

        jComboBoxColumns.addActionListener(actionListenerGridSizeChooser);
        jComboBoxRows.addActionListener(actionListenerGridSizeChooser);

        jComboBoxChoosePlate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                app.requestNewGrid();
            }
        });

        jSliderMouseHeat.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                double mouseTemperature = sliderValueToTemperature(jSliderMouseHeat.getValue(),
                        jSliderMouseHeat.getMaximum(), jSliderMouseHeat.getMinimum());
                app.setMouseTemperature(mouseTemperature);
                updateGUI();
            }
        });
    }

    public void updateGUI() {
        jLabelMouseTemperature.setText("Mouse Temperature: " + String.format("%.1f", app.getMouseTemperature()));
        hotPlateGUI.jLabelMouseHeatConstant.setText("Mouse Heat Constant: " + String.format("%.2f", elController.mouseHeatConstant));
        hotPlateGUI.jLabelElementHeatConstant.setText("Element Heat Constant: " + String.format("%.2f", elController.elementHeatConstant));
        jLabelElementSleepTime.setText("Element Sleep Time (ms): " + elController.elSleepTime);
    }

    public void adjustControlValues() {
        hotPlateGUI.jCheckBoxDrawBorder.setSelected(canvas.isDrawBorder());
        hotPlateGUI.jCheckBoxDrawHeatValues.setSelected(canvas.isDrawHeatValues());
    }

    private double sliderValueToTemperature(int value, int sliderMax, int sliderMin) {
        double sliderRatio = (value - sliderMin) / (double) (sliderMax - sliderMin);
        double heatRatio = sliderRatio * (Constants.TEMP_MAX - Constants.TEMP_MIN) + Constants.TEMP_MIN;
        return heatRatio;
    }

    //GETTERS / SETTERS
    public HotPlateGUI getHotPlateGUI() {
        return hotPlateGUI;
    }

    public MyCanvas getCanvas() {
        return canvas;
    }

    public JComboBox getjComboBoxRows() {
        return jComboBoxRows;
    }

    public JComboBox getjComboBoxColumns() {
        return jComboBoxColumns;
    }

    public JComboBox getjComboBoxChoosePlate() {
        return jComboBoxChoosePlate;
    }

    public JSlider getjSliderMouseHeat() {
        return jSliderMouseHeat;
    }

    public JSlider getjSliderHeatConstant() {
        return jSliderHeatConstant;
    }

    public JLabel getjLabelMouseTemperature() {
        return jLabelMouseTemperature;
    }

    public JLabel getjLabelHeatConstant() {
        return jLabelHeatConstant;
    }

    public JLabel getjLabelElementSleepTime() {
        return jLabelElementSleepTime;
    }
}
