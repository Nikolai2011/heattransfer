/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;

/**
 *
 * @author andrew
 */
public class HotPlateGUI extends JFrame {

    public JPanel jPanelTop;
    public javax.swing.JButton jButtonPause;
    public javax.swing.JButton jButtonResume;
    public javax.swing.JCheckBox jCheckBoxDrawBorder;
    public javax.swing.JCheckBox jCheckBoxDrawHeatValues;
    public javax.swing.JComboBox<String> jComboBoxChoosePlate;
    public javax.swing.JComboBox<String> jComboBoxColumns;
    public javax.swing.JComboBox<String> jComboBoxRows;
    public javax.swing.JLabel jLabelActualHeat;
    public javax.swing.JLabel jLabelChoosePlate;
    public javax.swing.JLabel jLabelColumns;
    public javax.swing.JLabel jLabelElementSleepTime;
    public javax.swing.JLabel jLabelExpectedHeat;
    public javax.swing.JLabel jLabelMouseHeatConstant;
    public javax.swing.JLabel jLabelElementHeatConstant;
    public javax.swing.JLabel jLabelMouseHeatTransferSpeed;
    public javax.swing.JLabel jLabelMouseTemp;
    public javax.swing.JLabel jLabelRedrawSpeed;
    public javax.swing.JLabel jLabelRows;
    public javax.swing.JPanel jPanelCanvas;
    public javax.swing.JPanel jPanelElementControlTools;
    public javax.swing.JPanel jPanelGridControlTools;
//    public javax.swing.JSlider jSliderHeatConstant;
    public javax.swing.JSlider jSliderMouseTemperature;
    public javax.swing.JTabbedPane jTabbedPaneTools;

    public HotPlateGUI() {
        this.setSize(600, 700);
        this.setLocationRelativeTo(null); //Show window in center

        initLayout();
        loadLayout();

    }

    private void initLayout() {
        jPanelTop = new JPanel(new BorderLayout());
        jPanelGridControlTools = new javax.swing.JPanel();
        jLabelRows = new javax.swing.JLabel();
        jLabelColumns = new javax.swing.JLabel();
        jComboBoxRows = new javax.swing.JComboBox<>();
        jComboBoxColumns = new javax.swing.JComboBox<>();
        jLabelChoosePlate = new javax.swing.JLabel();
        jComboBoxChoosePlate = new javax.swing.JComboBox<>();
        jCheckBoxDrawBorder = new javax.swing.JCheckBox();
        jPanelElementControlTools = new javax.swing.JPanel();
        jSliderMouseTemperature = new javax.swing.JSlider();
        jLabelMouseTemp = new javax.swing.JLabel();
//        jSliderHeatConstant = new javax.swing.JSlider();
        jLabelMouseHeatConstant = new javax.swing.JLabel();
        jLabelElementHeatConstant = new javax.swing.JLabel();
        jLabelElementSleepTime = new javax.swing.JLabel();
        jButtonPause = new javax.swing.JButton();
        jButtonResume = new javax.swing.JButton();
        jCheckBoxDrawHeatValues = new javax.swing.JCheckBox();
        jLabelExpectedHeat = new javax.swing.JLabel();
        jLabelActualHeat = new javax.swing.JLabel();
        jLabelRedrawSpeed = new javax.swing.JLabel();
        jLabelMouseHeatTransferSpeed = new javax.swing.JLabel();
        jTabbedPaneTools = new javax.swing.JTabbedPane();
        jPanelCanvas = new javax.swing.JPanel();

        jLabelRows.setText("Rows");
        jLabelColumns.setText("Columns");

        jComboBoxRows.setEditable(true);
        jComboBoxRows.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{"1", "2", "3", "5", "10", "20", "30", "50"}));

        jComboBoxColumns.setEditable(true);
        jComboBoxColumns.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{"1", "2", "3", "5", "10", "20", "30","50"}));

        jLabelChoosePlate.setText("Choose Plate");

        jComboBoxChoosePlate.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{"Cold Plate", "Medium", "Hot Plate"}));
        jComboBoxChoosePlate.setSelectedIndex(2);

        jCheckBoxDrawBorder.setSelected(true);
        jCheckBoxDrawBorder.setText("Draw Border");

        jPanelElementControlTools.setPreferredSize(new java.awt.Dimension(572, 150));

        jSliderMouseTemperature.setMaximum(1000);

        jLabelMouseTemp.setText("Mouse Temperature");

//        jSliderHeatConstant.setMinimum(1);
        jLabelMouseHeatConstant.setText("Heat Constant");
        jLabelMouseHeatConstant.setToolTipText("");

        jLabelElementSleepTime.setText("Element Sleep Time");

        jButtonResume.setText("Resume");
        jButtonPause.setText("Pause");
        jButtonPause.setPreferredSize(jButtonResume.getPreferredSize());

        jCheckBoxDrawHeatValues.setText("Draw Heat Values");

        jLabelExpectedHeat.setText("Expected Heat");

        jLabelActualHeat.setText("Actual Heat");

        jLabelRedrawSpeed.setText("Redraw Speed");
        jLabelMouseHeatTransferSpeed.setText("Mouse Heat Transfer Speed");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    }

    private void loadLayout() {
        int anchor = GridBagConstraints.CENTER;
        int fill = GridBagConstraints.HORIZONTAL;
        UIManager.getDefaults().put("TabbedPane.contentBorderInsets", new Insets(0, 0, 0, 0));
        Insets insets = new Insets(0, 10, 5, 5);

        JPanel jPanelLeft = new JPanel(new GridBagLayout());
        JPanel jPanelRight = new JPanel(new GridBagLayout());
        jPanelLeft.add(jButtonPause, new GridBagConstraints(0, 0, 1, 1, 0.0d, 0.0d, anchor, fill, insets, 0, 0));
        jPanelLeft.add(jButtonResume, new GridBagConstraints(1, 0, 1, 1, 0.0d, 0.0d, anchor, fill, insets, 0, 0));
        jPanelLeft.add(jCheckBoxDrawHeatValues, new GridBagConstraints(2, 0, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));

//        jPanelLeft.add(jSliderHeatConstant, new GridBagConstraints(0, 2, 3, 1, 1.0d, 0.0d, anchor, fill, new Insets(0, 10, 5, 5), 0, 0));
        jPanelLeft.add(jLabelMouseTemp, new GridBagConstraints(0, 3, 3, 1, 1.0d, 0.0d, anchor, fill, new Insets(5, 10, 0, 5), 0, 0));
        jPanelLeft.add(jSliderMouseTemperature, new GridBagConstraints(0, 4, 3, 1, 1.0d, 0.0d, anchor, fill, new Insets(0, 10, 5, 5), 0, 0));
        jPanelLeft.add(jLabelMouseHeatConstant, new GridBagConstraints(0, 5, 3, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));
        jPanelLeft.add(jLabelElementHeatConstant, new GridBagConstraints(0, 6, 3, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));

        insets = new Insets(0, 15, 5, 5);
        jPanelRight.add(jLabelExpectedHeat, new GridBagConstraints(0, 0, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));
        jPanelRight.add(jLabelActualHeat, new GridBagConstraints(0, 1, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));
        jPanelRight.add(jLabelElementSleepTime, new GridBagConstraints(0, 2, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));
        jPanelRight.add(jLabelMouseHeatTransferSpeed, new GridBagConstraints(0, 3, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));
        jPanelRight.add(jLabelRedrawSpeed, new GridBagConstraints(0, 4, 1, 1, 1.0d, 0.0d, anchor, fill, insets, 0, 0));

        jPanelElementControlTools.setLayout(new BorderLayout());

        JPanel fixer = new JPanel();
        fixer.add(jPanelLeft, BorderLayout.NORTH);
        jPanelElementControlTools.add(fixer, BorderLayout.WEST);
        jPanelElementControlTools.add(jPanelRight, BorderLayout.CENTER);

        JPanel jPanelJavaFix = new JPanel(new BorderLayout());
        insets = new Insets(0, 10, 5, 5);
        jPanelGridControlTools.setLayout(new GridBagLayout());
        jPanelGridControlTools.add(jLabelRows, new GridBagConstraints(0, 0, 1, 1, 1.0d, 0.0d, anchor, fill, new Insets(10, 10, 0, 5), 0, 0));
        jPanelGridControlTools.add(jLabelColumns, new GridBagConstraints(1, 0, 1, 1, 1.0d, 0.0d, anchor, fill, new Insets(10, 10, 0, 5), 0, 0));
        jPanelGridControlTools.add(jLabelChoosePlate, new GridBagConstraints(2, 0, 1, 1, 1.0d, 0.0d, anchor, fill, new Insets(10, 10, 0, 5), 0, 0));
        jPanelGridControlTools.add(jComboBoxRows, new GridBagConstraints(0, 1, 1, 1, 1.0d, 0.0d, anchor, fill, new Insets(0, 10, 10, 5), 0, 0));
        jPanelGridControlTools.add(jComboBoxColumns, new GridBagConstraints(1, 1, 1, 1, 1.0d, 0.0d, anchor, fill, new Insets(0, 10, 10, 5), 0, 0));
        jPanelGridControlTools.add(jComboBoxChoosePlate, new GridBagConstraints(2, 1, 1, 1, 1.0d, 0.0d, anchor, fill, new Insets(0, 10, 10, 5), 0, 0));
        jPanelGridControlTools.add(jCheckBoxDrawBorder, new GridBagConstraints(3, 0, 1, 2, 1.0d, 0.0d, anchor, fill, new Insets(10, 20, 10, 10), 0, 0));
        jPanelJavaFix.add(jPanelGridControlTools, BorderLayout.NORTH);

        jTabbedPaneTools.addTab("Element Control Tools", jPanelElementControlTools);
        jTabbedPaneTools.addTab("Grid Control Tools", jPanelJavaFix);

        jPanelTop.add(jPanelCanvas, BorderLayout.CENTER);
        jPanelTop.add(jTabbedPaneTools, BorderLayout.SOUTH);
        getContentPane().add(jPanelTop);
    }
}
